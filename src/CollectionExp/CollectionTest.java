package CollectionExp;
import java.util.*;
public class CollectionTest {
	public static void main(String[] args) {
		int choice=0;
		
		ArrayList<Employee> e = new ArrayList<Employee>();
		e.add(new Employee(1101));
		while(choice !=6) {
			System.out.println("------------\nChoose an option:\n1. Add Employee\n2. Display All\n3. Search Employee\n4. Delete Employee\n5. Update\n");
			
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
			 Iterator<Employee> ip = e.iterator();
			
			 switch(choice) {
			 case 1: 
				 
				System.out.println("Enter empid to add: ");
				Scanner emp = new Scanner(System.in);
				int eid = emp.nextInt();
				e.add(new Employee(eid));
				System.out.println("Employee added successfully ");
				break;
				
			 case 2:
				
				System.out.println("All employee ids are : ");
				while(ip.hasNext()) {
					
					System.out.println( ip.next().empid);
				}
				break;
				
			 case 3:
				 
				 System.out.println("Enter empid to search: ");
				 Scanner emp1 = new Scanner(System.in);
				 int eid1 = emp1.nextInt();
				 int found = 0;
				 while(ip.hasNext()) {
					 if(ip.next().empid == eid1) {
						 System.out.println("Employee Found");
						 found++;
						 break;
					 }
				 }
				 if(found == 0) {
					 
					 System.out.println("Employee not found");
				 }
				 break;
				 
			 case 4: 
		
				 System.out.println("Enter empid to Delete: ");
				 Scanner emp2 = new Scanner(System.in);
				 int eid2 = emp2.nextInt();
				 int del = 0;
				 while(ip.hasNext()) {
					 if(ip.next().empid == eid2) {
						 del =1;
						 ip.remove();
						 System.out.println("Employee "+eid2+" Deleted");
						 break;
					 }
				 }
				 if(del==0) {
					 System.out.println("Employee not found");
				 } 
				 break;
				
			 case 5: 
					
				 System.out.println("Enter empid to Update: ");
				 Scanner emp3 = new Scanner(System.in);
				 int eid3 = emp3.nextInt();
				 int upd = 0;
				 while(ip.hasNext()) {
					 if(ip.next().empid == eid3) {
						 upd =1;
						
						 System.out.println("Employee "+eid3+" Updated");
						 break;
					 }
				 }
				 if(upd==0) {
					 System.out.println("Employee not found");
				 } 
				 break;
			default : 			
				
				System.out.println("Invalid Choice");
			 }
		
			
			
			
			
		}
		
	}

}
